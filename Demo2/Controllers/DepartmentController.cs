﻿using Demo2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Demo2.Controllers
{
    public class DepartmentController : Controller
    {
        SchoolDB db = new SchoolDB();
        // GET: Department

        // read All Departments
        public ViewResult Index()
        {
            List<Department> depts = db.Departments.ToList<Department>();
            return View(depts);
        }
        // create
        [HttpGet]
        public ViewResult create()
        {
            return View();
        }
        // department/create?deptid=7&deptname=iti&capactiy=800
        // int deptid, string deptname,int capactiy
        [HttpPost]
        public RedirectToRouteResult Create(Department dept)
        {
            //Department dept = new Department() { deptid = deptid, deptname = deptname, capactiy = capactiy };
            try
            {
                if (ModelState.IsValid)
                {
                    db.Departments.Add(dept);
                    db.SaveChanges();
                }
            }
            catch
            {
                ModelState.AddModelError("", "unable to save changes .Try again");
            }
            return RedirectToAction("Index");
            //return View("CreateConfermid",dept);
        }
        public ViewResult showcreate()
        {
            return View();
        }

        //Detail
        public ActionResult Detailes(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            Department dept = db.Departments.Find(id);
            if(dept == null) 
            { 
                return HttpNotFound(); 
            }
            return View(dept);
        }

        // Edit 
        [HttpPost]
        public ActionResult Edit(Department dept)

        {
            Department olddept = db.Departments.Find(dept.deptid);
            olddept.deptname = dept.deptname;
            olddept.capactiy = dept.capactiy;
            db.SaveChanges();
            return RedirectToAction("index");
        }
        [HttpGet]
        public ViewResult Edit(int id)
        {
            Department dept = db.Departments.Find(id);
            return View(dept);
        }

        // Delete
        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            Department dept = db.Departments.Find(id);
            if (dept == null)
            {
                return HttpNotFound();
            }
            return View(dept);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                Department dept = db.Departments.Find(id);
                db.Departments.Remove(dept);
                db.SaveChanges();
            }
            catch
            {
                ModelState.AddModelError("", "unable to save changes .Try again");
            }
            return RedirectToAction("Index");
        }


    }
}